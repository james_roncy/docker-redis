const mainApp = {
    name        : "redis-commander",
    script      : "./app.js"
}

module.exports = { apps : [mainApp] }